//
//  UserModel.swift
//  CTTracker
//
//  Created by Apple on 11/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class UserModel: NSObject {

    var strUserID = ""
    var strName = ""
    var strEmail = ""
    var strMobile = ""
    var strImage = ""
}
