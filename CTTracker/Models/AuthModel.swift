//
//  AuthModel.swift
//  Tribe365
//
//  Created by kdstudio on 30/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

class AuthModel {

    static let sharedInstance = AuthModel()

    var email = ""
    var user_id = ""
    var mobile = ""
    var name = ""
    var userType = ""
    var image = ""
    var orgId = ""
    var locationOn = false
    var createdAt = ""
    var deviceToken = ""
    var latitude = ""
    var longitude = ""
    var password = ""
    var address = ""
    var website = ""
}
