//
//  AppDelegate.swift
//  CTTracker
//
//  Created by Apple on 07/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().barTintColor = UIColor(red: 236.0/255.0, green: 28.0/255.0, blue: 45.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]

        GMSServices.provideAPIKey("AIzaSyBUuMuzCUSW6N8CP5UkvijQgHODMLLqRjo")
        
        //        application.setMinimumBackgroundFetchInterval(10)

       /* if !AuthModel.sharedInstance.user_id.isEmpty
        {
            if UserDefaults.standard.value(forKey: "user_type") as! String == "3" {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let rootController = storyboard.instantiateViewController(withIdentifier: "userLogin")
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = rootController
                self.window?.makeKeyAndVisible()
            }
            else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let rootController = storyboard.instantiateViewController(withIdentifier: "userListNav")
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = rootController
                self.window?.makeKeyAndVisible()
            }
        }*/
        
        if AuthModel.sharedInstance.user_id.isEmpty {
            if UserDefaults.standard.value(forKey: "user_id") != nil {
                
                //direct login
                let defaults = UserDefaults.standard
                let auth = AuthModel.sharedInstance
                
                auth.email = defaults.value(forKey: "email") as! String
                auth.user_id = defaults.value(forKey: "user_id") as! String
                auth.mobile = defaults.value(forKey: "mobile") as! String
                auth.name = defaults.value(forKey: "name") as! String
                auth.userType = defaults.value(forKey: "user_type") as! String
                auth.orgId = defaults.value(forKey: "organisation_id") as! String
                auth.image = defaults.value(forKey: "image") as! String
                auth.address = defaults.value(forKey: "address") as! String
                auth.website = defaults.value(forKey: "website") as! String


//                callWebServiceForLoginStatus()
                    if UserDefaults.standard.value(forKey: "user_type") as! String == "3" {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let rootController = storyboard.instantiateViewController(withIdentifier: "userLogin")
                        self.window = UIWindow(frame: UIScreen.main.bounds)
                        self.window?.rootViewController = rootController
                        self.window?.makeKeyAndVisible()
                    }
                    else {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let rootController = storyboard.instantiateViewController(withIdentifier: "userListNav")
                        self.window = UIWindow(frame: UIScreen.main.bounds)
                        self.window?.rootViewController = rootController
                        self.window?.makeKeyAndVisible()
                    }
            }
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
//        if !AuthModel.sharedInstance.user_id.isEmpty {
//            callWebServiceForLoginStatus()
//        }
        if (UserDefaults.standard.value(forKey: "user_id") != nil) && (UserDefaults.standard.value(forKey: "user_type") as? String != "2") {
            callWebServiceForLoginStatus()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    //MARK: - CallWebService
    func callWebServiceForLoginStatus() {
        
        let param = ["user_id": AuthModel.sharedInstance.user_id, "device_id":UIDevice.current.identifierForVendor!.uuidString] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.loginStatus, param: param, withHeader: false ) { (response, errorMsg) in
            
            if response == nil {
            }
            else{
                if response!["code"].stringValue == "200" && response!["data"]["logged_in"].stringValue == "no" {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let rootController = storyboard.instantiateViewController(withIdentifier: "loginNav")
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    self.window?.rootViewController = rootController
                    self.window?.makeKeyAndVisible()
                }
//                else {
//                    if UserDefaults.standard.value(forKey: "user_type") as! String == "3" {
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let rootController = storyboard.instantiateViewController(withIdentifier: "userLogin")
//                        self.window = UIWindow(frame: UIScreen.main.bounds)
//                        self.window?.rootViewController = rootController
//                        self.window?.makeKeyAndVisible()
//                    }
//                    else {
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let rootController = storyboard.instantiateViewController(withIdentifier: "userListNav")
//                        self.window = UIWindow(frame: UIScreen.main.bounds)
//                        self.window?.rootViewController = rootController
//                        self.window?.makeKeyAndVisible()
//                    }
//
//                }
            }
            }
        }
    
//    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//
//
//        var timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
//
////        // fetch data from internet now
////        guard let data = fetchSomeData() else {
////            // data download failed
////            completionHandler(.failed)
////            return
////        }
////
////        if data.isNew {
////            // data download succeeded and is new
////            completionHandler(.newData)
////        } else {
////            // data downloaded succeeded and is not new
////            completionHandler(.noData)
////        }
//    }
//
//    //MARK: - Custom Method
//    @objc func updateLocation() {
//        let locationManager = CLLocationManager()
//        let location: CLLocation = locationManager.location!
//
//        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
//            if (error != nil) {
//                print("Reverse geocoder failed with error" + error!.localizedDescription)
//                return
//            }
//
//            if placemarks!.count != 0 {
//                let pm = placemarks![0] as CLPlacemark
//                if pm != nil {
//                    //stop updating location to save battery life
//                    //                    locationManager.stopUpdatingLocation()
//                    let locality = (pm.locality != nil) ? pm.locality ?? "" : ""
//                    let postcode = (pm.postalCode != nil) ? pm.postalCode ?? "" : ""
//                    let admin = (pm.administrativeArea != nil) ? pm.administrativeArea ?? "" : ""
//                    let country = (pm.country != nil) ? pm.country ?? "" : ""
//
//                    self.callWebServiceForSaveLocation(latitude: String(location.coordinate.latitude), longitude: String(location.coordinate.longitude), address: "App Terminated")
//
//                    //                    self.callWebServiceForSaveLocation(latitude: String(location.coordinate.latitude), longitude: String(location.coordinate.longitude), address: String(locality + "" + postcode + ", " + admin + " " + country))
//                }
//            } else {
//                print("Problem with the data received from geocoder")
//            }
//        })
//    }
//
//    //MARK: - CallWebService
//    func callWebServiceForSaveLocation(latitude: String, longitude: String, address: String) {
//
//        let param = ["user_id": AuthModel.sharedInstance.user_id,
//                     "latitude":latitude,
//                     "longitude": longitude,
//                     "address":address] as [String : Any]
//
//        print(kBaseURL + NetworkConstant.saveLocation)
//        print(param)
//
//        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.saveLocation, param: param, withHeader: false ) { (response, errorMsg) in
//
//        }
//    }
}

