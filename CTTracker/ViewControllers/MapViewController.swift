//
//  MapViewController.swift
//  CTTracker
//
//  Created by Apple on 16/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import MapKit
import JKNotificationPanel
import NVActivityIndicatorView

class MapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!

    var panel = JKNotificationPanel()
    var strSelectedDate = ""
    var arrLocations = [LocationModel]()
    var strSelectedUserID = ""
    let locations = [
        ["image": #imageLiteral(resourceName: "pin"),    "latitude": 35.711015, "longitude": 139.796375],
        ["image": #imageLiteral(resourceName: "pin"),  "latitude": 35.713825, "longitude": 139.796608],
        ["image": #imageLiteral(resourceName: "pin"),     "latitude": 35.714302, "longitude": 139.796513],
        ["image": #imageLiteral(resourceName: "pin"),     "latitude": 35.714638, "longitude": 139.797491]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callWebServiceForLocationList()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "LOGOUT", message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //run your function here
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            AuthModel.sharedInstance.user_id = ""
            AuthModel.sharedInstance.email = ""
            AuthModel.sharedInstance.mobile = ""
            AuthModel.sharedInstance.name = ""
            AuthModel.sharedInstance.userType = ""
            AuthModel.sharedInstance.image = ""
            AuthModel.sharedInstance.orgId = ""
            AuthModel.sharedInstance.createdAt = ""
            AuthModel.sharedInstance.deviceToken = ""
            AuthModel.sharedInstance.latitude = ""
            AuthModel.sharedInstance.longitude = ""
            AuthModel.sharedInstance.password = ""
            AuthModel.sharedInstance.address = ""
            AuthModel.sharedInstance.website = ""
            AuthModel.sharedInstance.website = ""
            
            let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "loginNav")
            self.appDelegate.window!.rootViewController = centerVC
            self.appDelegate.window!.makeKeyAndVisible()
            
            //            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
    }
    @IBAction func btnUserListAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    
    }
  
    
    func addAnnotations() {
        
        for (index,arrLocations) in arrLocations.enumerated() {
            let annotation = MKPointAnnotation()
        
            annotation.title =  arrLocations.strDate
            annotation.subtitle = arrLocations.strAddress
            annotation.coordinate = CLLocationCoordinate2D(latitude: Double(arrLocations.strLatitude)!, longitude: Double(arrLocations.strLongitude)!)
            mapView.addAnnotation(annotation)
        }
        
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        
    }

    
    //MARK: - CallWebService
    func callWebServiceForLocationList() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.getUserLocationMap, param: ["user_id":strSelectedUserID, "date":  strSelectedDate], withHeader: false) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["code"].stringValue == "200" {
                    
                    ParserController.parseLocationMapList(response: response!, completionHandler: { (arr) in
                        self.arrLocations = arr
                        self.addAnnotations()

                    })
                    
//                    self.lblNoDate.isHidden = true
//                    self.tblView.isHidden = false
//                    self.tblView.reloadData()
                }
                else if response!["code"].stringValue == "400" {
                    
//                    self.pageNo = 1
//                    self.lblNoDate.isHidden = false
//                    self.arrLocations.removeAll()
//                    self.tblView.isHidden = true
//                    self.tblView.reloadData()
//                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
   
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView)
    {
        if let annotationTitle = view.annotation?.title
        {
            print("User tapped on annotation with title: \(annotationTitle!)")
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "MyPin"
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            
            annotationView?.image = #imageLiteral(resourceName: "Annotation")
          
            /*locations[Int((annotation.title ?? "0")!)!]["image"] as? UIImage*/
            // if you want a disclosure button, you'd might do something like:
            //
            // let detailButton = UIButton(type: .detailDisclosure)
            // annotationView?.rightCalloutAccessoryView = detailButton
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
}
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
    }
}


