//
//  LoginView.swift
//  CTTracker
//
//  Created by Apple on 10/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import SwiftyJSON

class LoginView: UIViewController, UITextFieldDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmailFP: UITextField!

    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var forgotPasswordView: UIView!
    @IBOutlet weak var loginView: UIView!

    //MARK: - Variables
    let panel = JKNotificationPanel()

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtEmail.placeHolderColor = UIColor.darkGray
        txtPassword.placeHolderColor = UIColor.darkGray
        
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)

    }
    
    //MARK: - Custom Methods
    func setLoginData(json: JSON) {
               
        let auth = AuthModel.sharedInstance
        auth.email = json["data"]["email"].stringValue
        auth.user_id = json["data"]["id"].stringValue
        auth.mobile = json["data"]["mobile"].stringValue
        auth.name = json["data"]["name"].stringValue
        auth.userType = json["data"]["user_type"].stringValue
        auth.orgId = json["data"]["organisation_id"].stringValue
        auth.image = json["data"]["image"].stringValue
        auth.address = json["data"]["address"].stringValue
        auth.website = json["data"]["website"].stringValue
      
        
        let defaults = UserDefaults.standard
        defaults.set(auth.email, forKey: "email")
        defaults.set(auth.user_id, forKey: "user_id")
        defaults.set(auth.mobile, forKey: "mobile")
        defaults.set(auth.name, forKey: "name")
        defaults.set(auth.userType, forKey: "user_type")
        defaults.set(auth.orgId, forKey: "organisation_id")
        defaults.set(auth.image, forKey: "image")
        defaults.set(auth.address, forKey: "address")
        defaults.set(auth.website, forKey: "website")
        
    }

    //MARK: - IBActions
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        loginView.isHidden = true
        forgotPasswordView.isHidden = false
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        loginView.isHidden = false
        forgotPasswordView.isHidden = true
    }

    
    @IBAction func btnSignUpAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterCompanyView") as! RegisterCompanyView
        self.navigationController?.pushViewController(objVC, animated: true)
        
      /*  let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationView") as! RegistrationView
        self.navigationController?.pushViewController(objVC, animated: true)*/
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        if (txtEmailFP.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter email.")
        }
        else if (txtEmailFP.text?.isValidEmail() == false) {
            
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid email.")
        }
        else {
            callWebServiceForForgotPassword()
        }
    }
    
    @IBAction func btnSignInAction(_ sender: Any) {
        if (txtEmail.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter email.")
        }
        else if (txtEmail.text?.isValidEmail() == false) {
            
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid email.")
        }
        else if (txtPassword.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter password.")
        }
        else {
            callWebServiceForLogin()
        }
    }
    
    //MARK: - UITextfiled Delegete
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.placeHolderColor = UIColor.darkGray

        if textField == txtEmail{
            
            viewUserName.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
        else if textField == txtPassword{
            
            viewPassword.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.placeHolderColor = UIColor.darkGray

        if textField == txtEmail{
            
            viewUserName.borderColor = UIColor.lightGray
        }
        else if textField == txtPassword{
            
            viewPassword.borderColor = UIColor.lightGray
        }
        
    }
    
   
    //MARK: - CallWebService
    func callWebServiceForLogin() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["email": txtEmail.text!, "password":txtPassword.text ?? "", "device_id": UIDevice.current.identifierForVendor!.uuidString] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.login, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["code"].stringValue == "200" {
                    self.setLoginData(json: response!)
                    
                    if UserDefaults.standard.value(forKey: "user_type") as! String == "3" {
                        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LocationSwitchView") as! LocationSwitchView
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else {
                        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserListView") as! UserListView
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForForgotPassword() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["email": txtEmailFP.text!] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.forgotPassword, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: errorMsg)
                    self.loginView.isHidden = false
                    self.forgotPasswordView.isHidden = true
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}

//extension UIColor {
//    convenience init(hexString: String) {
//        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
//        var int = UInt32()
//        Scanner(string: hex).scanHexInt32(&int)
//        let a, r, g, b: UInt32
//        switch hex.count {
//        case 3: // RGB (12-bit)
//            (a, r, g, b) = (255, (int >> 8)  17, (int >> 4 & 0xF)  17, (int & 0xF) * 17)
//        case 6: // RGB (24-bit)
//            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
//        case 8: // ARGB (32-bit)
//            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
//        default:
//            (a, r, g, b) = (255, 0, 0, 0)
//        }
//        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
//    }
//}
