//
//  LocationSwitchView.swift
//  
//
//  Created by Apple on 10/12/18.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import SwiftyJSON
import CoreLocation
import AddressBookUI
import Contacts
import GoogleMaps

class LocationSwitchView: UIViewController, CLLocationManagerDelegate {

    //MARK: - Variables
    let locationManager = CLLocationManager()
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    var timer = Timer()
    var startTime: Date? //An instance variable, will be used as a previous location time.

    //MARK: - IBOutlets
    @IBOutlet weak var lblTracker: UILabel!
    @IBOutlet weak var locationSwitch: UISwitch!
    @IBOutlet weak var imgLogo: UIImageView!
    
    var mapView: GMSMapView!

    //MARK: - View Controller
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "LOCATION"
        self.navigationItem.setHidesBackButton(true, animated: false)

        //Your map initiation code
//        mapView = GMSMapView.map(withFrame: CGRect.init(x: 0, y: 0, width: 0, height: 0), camera: GMSCameraPosition.init())
////        self.view = mapView
//        self.mapView?.isMyLocationEnabled = true
        

        if AuthModel.sharedInstance.locationOn || CLLocationManager.locationServicesEnabled() {
            locationSwitch.isOn = true
            lblTracker.isHidden = false
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 80.0;
            locationManager.requestWhenInUseAuthorization()
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.startUpdatingLocation()

            timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
            registerBackgroundTask()
            

        }

        self.locationManager.requestAlwaysAuthorization()

    }
    
    //MARK: - IBAction
    
    @IBAction func btnEditAction(_ sender: Any) {
        
        
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "LOGOUT", message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //run your function here
            self.timer.invalidate()
            self.locationManager.stopUpdatingLocation()
            self.lblTracker.isHidden = true
            AuthModel.sharedInstance.locationOn = false
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            AuthModel.sharedInstance.user_id = ""
            self.endBackgroundTask()
            let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "loginNav")
            self.appDelegate.window!.rootViewController = centerVC
            self.appDelegate.window!.makeKeyAndVisible()

//            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)

    }
    
    @IBAction func locationSwitchAction(_ sender: UISwitch) {
        
        if sender.isOn {
            enableLocationService()
            registerBackgroundTask()
        }
        else {
//            timer.invalidate()
            locationManager.stopUpdatingLocation()
            lblTracker.isHidden = true
            AuthModel.sharedInstance.locationOn = false
            UserDefaults.standard.set(false, forKey: "locationOn")
            endBackgroundTask()
        }
    }
    
    //MARK: - Custom Methods
    @objc func updateLocation() {
        let location: CLLocation = locationManager.location!
//        if mapView == nil {
//            let camera = GMSCameraPosition.camera(withLatitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!, zoom: 0)
//            mapView = GMSMapView.map(withFrame: .zero, camera: camera)
//        }
//        let location = CLLocation.init(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)

        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }

            if placemarks!.count != 0 {
                let pm = placemarks![0] as CLPlacemark
                if pm != nil {

//                    self.callWebServiceForSaveLocation(latitude: String(location.coordinate.latitude), longitude: String(location.coordinate.longitude), address: ABCreateStringWithAddressDictionary(pm.addressDictionary ?? [:], false))

                    self.callWebServiceForSaveLocation(latitude: String(location.coordinate.latitude), longitude: String(location.coordinate.longitude), address: String("\(pm.subLocality!), \(pm.locality!), \(pm.administrativeArea!), \(pm.postalCode!), \(pm.country!)"))
                    
                    
                }
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }

    func enableLocationService() {
        

        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.distanceFilter = 0.0;
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()

        }
        
        //locationManager.requestWhenInUseAuthorization()
        let authorizationStatus = CLLocationManager.authorizationStatus()
        
        let selector = #selector(self.locationManager.requestWhenInUseAuthorization)
        if self.locationManager.responds(to:selector) {
            
            if authorizationStatus == .authorizedAlways
                || authorizationStatus == .authorizedWhenInUse {
                
                self.locationManager.startUpdatingLocation()
                lblTracker.isHidden = false
                AuthModel.sharedInstance.locationOn = true
                UserDefaults.standard.set(true, forKey: "locationOn")

                timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
            }else{
                
                self.locationManager.requestWhenInUseAuthorization()
            }
            
        }else{
            self.locationManager.startUpdatingLocation()
            lblTracker.isHidden = false
            AuthModel.sharedInstance.locationOn = true
            UserDefaults.standard.set(true, forKey: "locationOn")
            timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)

        }
    }
    
    func registerBackgroundTask() {
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
//            self?.endBackgroundTask()
            self?.timer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(self?.updateLocation), userInfo: nil, repeats: true)
        }
        assert(backgroundTask != UIBackgroundTaskInvalid)
        
//        UIApplication.shared.setMinimumBackgroundFetchInterval(900)

    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("locationManager-failed")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            // you're good to go!
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didUpdateLocations called")
    }

    //MARK: - CallWebService
    func callWebServiceForSaveLocation(latitude: String, longitude: String, address: String) {
        
        let param = ["user_id": AuthModel.sharedInstance.user_id,
                     "latitude":latitude,
                     "longitude": longitude,
                     "address":address] as [String : Any]
        
        print(kBaseURL + NetworkConstant.saveLocation)
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.saveLocation, param: param, withHeader: false ) { (response, errorMsg) in
            
            }
    }
}
