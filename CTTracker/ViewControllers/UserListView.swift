//
//  UserListView.swift
//  
//
//  Created by Apple on 10/12/18.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import Kingfisher
class UserListView: UIViewController, UITableViewDelegate, UITableViewDataSource{

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgLogo: UIImageView!
    
    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrUsers = [UserModel]()
    var pageNo = 1
    var loadMore = true

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.delegate = self
        tblView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let url = URL(string: KImageURL + AuthModel.sharedInstance.image)
        
        imgLogo.kf.setImage(with: url, placeholder: UIImage(named : "placeholder"), progressBlock:nil, completionHandler: nil)
        
        self.title = "USER LIST"
        self.navigationItem.setHidesBackButton(true, animated: false)

        pageNo = 1
        loadMore = true
        arrUsers.removeAll()
        callWebServiceForUserList()

    }
    
    //MARK: - IBAction
    
    
    @IBAction func btnEditProfileAction(_ sender: Any) {
        
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpdateCompanyDetails") as! UpdateCompanyDetails
        objVC.strUpdate = "True"
        self.navigationController?.pushViewController(objVC, animated: true)
        
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "LOGOUT", message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            //run your function here
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            AuthModel.sharedInstance.user_id = ""
            AuthModel.sharedInstance.email = ""
            AuthModel.sharedInstance.mobile = ""
            AuthModel.sharedInstance.name = ""
            AuthModel.sharedInstance.userType = ""
            AuthModel.sharedInstance.image = ""
            AuthModel.sharedInstance.orgId = ""
            AuthModel.sharedInstance.createdAt = ""
            AuthModel.sharedInstance.deviceToken = ""
            AuthModel.sharedInstance.latitude = ""
            AuthModel.sharedInstance.longitude = ""
            AuthModel.sharedInstance.password = ""
            AuthModel.sharedInstance.address = ""
            AuthModel.sharedInstance.website = ""
            AuthModel.sharedInstance.website = ""

            let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "loginNav")
            self.appDelegate.window!.rootViewController = centerVC
            self.appDelegate.window!.makeKeyAndVisible()
            
            //            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }

    //MARK:- UItalbeView Delegate and Dataq Sources
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UserListCell = tableView.dequeueReusableCell(withIdentifier: "UserListCell") as! UserListCell
        cell.selectionStyle = .none
         cell.lblName.text = arrUsers[indexPath.row].strName
         cell.lblEmail.text = arrUsers[indexPath.row].strEmail
         cell.lblMobileNumber.text = arrUsers[indexPath.row].strMobile
        
        let url = URL(string: KImageURL + arrUsers[indexPath.row].strImage)
        
        cell.imgProfile.layer.cornerRadius = 40.0
        cell.imgProfile.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        cell.imgProfile.borderWidth = 1.0
        cell.imgProfile.kf.setImage(with: url, placeholder: UIImage(named : "placeholder"), progressBlock:nil, completionHandler: nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 115
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LocationListView") as! LocationListView
        objVC.selectedUserID = arrUsers[indexPath.row].strUserID
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if loadMore {
            
            let lastElement = arrUsers.count - 1
            if indexPath.row == lastElement {
                // handle your logic here to get more items
                self.callWebServiceForUserList()
            }
        }
    }
    /*Images Base url
     http://production.chetaru.co.uk/ct_tracker/public/uploads/[image_name]*/
   
    @IBAction func btnAddAction(_ sender: UIButton) {
        
        if arrUsers.count > 4{
            
            let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PaymentDialogView") as! PaymentDialogView
            objVC.modalPresentationStyle = .overCurrentContext
            self.present(objVC, animated: false, completion: nil)
          
            
        }
        else{
        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationView") as! RegistrationView
        self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    //MARK: - CallWebService
    func callWebServiceForUserList() {
        
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        print("PageNumber: ",pageNo)
        
        WebServiceHandler.getWebService(url: kBaseURL + NetworkConstant.userList + String(pageNo) + "&organisation_id=" + AuthModel.sharedInstance.orgId , param: nil, withHeader: false) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["code"].stringValue == "200" {
                    ParserController.parseUserList(response: response!, completionHandler: { (arr, total) in
                        self.arrUsers.append(contentsOf: arr) 
                        
                        if Int(total) == self.arrUsers.count {
                            self.loadMore = false
                        }
                        else {
                            self.pageNo = self.pageNo + 1
                        }
                    })
                    
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
}
