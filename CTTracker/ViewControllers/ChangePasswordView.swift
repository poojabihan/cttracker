//
//  ChangePasswordView.swift
//  CTTracker
//
//  Created by Apple on 09/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class ChangePasswordView: UIViewController, UITextFieldDelegate {

    //MARK: - IBOutlets
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var viewCurrentPassword: UIView!
    @IBOutlet weak var viewNewPassword: UIView!
    @IBOutlet weak var viewConfirmPassword: UIView!
    
    
    @IBOutlet weak var btnNewPasswordEye: UIButton!
    @IBOutlet weak var btnConfirmPasswordEye: UIButton!
    @IBOutlet weak var btnCurrentPasswordEye: UIButton!
    
    //MARK: - Variables
    let panel = JKNotificationPanel()
    var textField: UITextField?
    var iconClickPass = true
    var iconClickConfirmPass = true
    var iconClickOldPass = true
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Notification Observers for UIKeyboard Hid Unhide

        txtCurrentPassword.placeHolderColor = UIColor.darkGray
        txtNewPassword.placeHolderColor = UIColor.darkGray
        txtConfirmPassword.placeHolderColor = UIColor.darkGray
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()
        
        btnNewPasswordEye.isSelected = true
        btnConfirmPasswordEye.isSelected = true
        btnCurrentPasswordEye.isSelected = true
        // Do any additional setup after loading the view.
    }
    
    
    //MARK: - IBActions
    
    @IBAction func iconActionOldPassword(sender: AnyObject) {
        
        if(iconClickOldPass == true) {
            txtCurrentPassword.isSecureTextEntry = false
            btnCurrentPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnCurrentPasswordEye.isSelected = false
        } else {
            txtCurrentPassword.isSecureTextEntry = true
            btnCurrentPasswordEye.setImage(#imageLiteral(resourceName: "unHide"), for: .normal)
            btnCurrentPasswordEye.isSelected = true
            
        }
        
        iconClickOldPass = !iconClickOldPass
    }
    
    @IBAction func iconActionPassword(sender: AnyObject) {
        if(iconClickPass == true) {
            txtNewPassword.isSecureTextEntry = false
            btnNewPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnNewPasswordEye.isSelected = false
        } else {
            txtNewPassword.isSecureTextEntry = true
            btnNewPasswordEye.setImage(#imageLiteral(resourceName: "unHide"), for: .normal)
            btnNewPasswordEye.isSelected = true
            
        }
        
        iconClickPass = !iconClickPass
    }
    
    @IBAction func iconActionConfirmPassword(sender: AnyObject) {
        
        if(iconClickConfirmPass == true) {
            txtConfirmPassword.isSecureTextEntry = false
            btnConfirmPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnConfirmPasswordEye.isSelected = false
        } else {
            txtConfirmPassword.isSecureTextEntry = true
            btnConfirmPasswordEye.setImage(#imageLiteral(resourceName: "unHide"), for: .normal)
            btnConfirmPasswordEye.isSelected = true
        }
        
        iconClickConfirmPass = !iconClickConfirmPass
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        
        if (txtCurrentPassword.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter your current password.")
        }
        else if(checkForSpace(strCheckString: txtCurrentPassword.text!) ==  true){
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
        }
        else if (txtNewPassword.text?.isEmpty)! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password should not blank.")
        }
        else if(checkForSpace(strCheckString: txtNewPassword.text!) ==  true){
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
        }
        else if txtConfirmPassword.text! != txtNewPassword.text! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password does not match the confirm password.")
        }
            
            
        else{
            
            CallWebServiceToChangePassword();
            
        }
    }
    
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
        
    }
    
    
    
    
    @IBAction func btnCrossAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    
    //MARK: - Calling web service
    func CallWebServiceToChangePassword(){
        
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        /*{
         
         "userId":"74",
         "password":"123",
         "oldPassword":"1234
         }
         */
        let param =
            [ "userId":AuthModel.sharedInstance.user_id,
              "password":txtNewPassword.text!,
              "oldPassword": txtCurrentPassword.text! ] as [String : Any]
        
        print(param)
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.userUpdatePassword , param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    print("SUCCESS Add")
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: errorMsg)
                    self.dismiss(animated: false, completion: nil)
                }
                else {
                    print(errorMsg ?? "")
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    
    //MARK: - UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.placeHolderColor = UIColor.darkGray

        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
        if textField == txtCurrentPassword{
            
            viewCurrentPassword.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
        else if textField == txtNewPassword{
            
            viewNewPassword.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
            
        else if textField == txtConfirmPassword{
            
            viewConfirmPassword.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
        
        if textField == txtCurrentPassword{
            
            viewCurrentPassword.borderColor = UIColor.lightGray
        }
        else if textField == txtNewPassword{
            
            viewNewPassword.borderColor = UIColor.lightGray
        }
            
        else if textField == txtConfirmPassword{
            
            viewConfirmPassword.borderColor = UIColor.lightGray
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
  

}
