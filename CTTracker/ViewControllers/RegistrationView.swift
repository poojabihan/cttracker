//
//  RegistrationView.swift
//  CTTracker
//
//  Created by Apple on 10/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel
import CoreLocation
import SwiftyJSON

class RegistrationView: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var tblView: UITableView!

    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPhoneNo: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewConfirmPassword: UIView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    
    @IBOutlet weak var btnPasswordEye: UIButton!
    @IBOutlet weak var btnConfirmPasswordEye: UIButton!
    
    //MARK: - Variables
    let panel = JKNotificationPanel()
    let locationManager = CLLocationManager()
    var locValue = CLLocationCoordinate2D()
    var textField: UITextField?
    var imagePicker = UIImagePickerController()
    var hideBackBtn = false
    var isProfilePictureSelected = false
    var iconClickPass = true
    var iconClickConfirmPass = true
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
        imagePicker.delegate = self

        // Do any additional setup after loading the view.
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        self.title = "ADD USER"
        if hideBackBtn {
            self.navigationItem.setHidesBackButton(true, animated: false)
        }
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        btnPasswordEye.isSelected = true
        btnConfirmPasswordEye.isSelected = true
        
        txtEmail.placeHolderColor = UIColor.darkGray
        txtMobileNumber.placeHolderColor = UIColor.darkGray
        txtName.placeHolderColor = UIColor.darkGray
        txtPassword.placeHolderColor = UIColor.darkGray
        txtConfirmPassword.placeHolderColor = UIColor.darkGray
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Notification Observers for UIKeyboard Hid Unhide
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
        self.hideKeyboardWhenTappedAround()

    }
    
    //MARK: - IBActions
    
    @IBAction func iconActionPassword(sender: AnyObject) {
        if(iconClickPass == true) {
            txtPassword.isSecureTextEntry = false
            btnPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnPasswordEye.isSelected = false
        } else {
            txtPassword.isSecureTextEntry = true
            btnPasswordEye.setImage(#imageLiteral(resourceName: "unHide"), for: .normal)
            btnPasswordEye.isSelected = true
            
        }
        
        iconClickPass = !iconClickPass
    }
    
    @IBAction func iconActionConfirmPassword(sender: AnyObject) {
        if(iconClickConfirmPass == true) {
            txtConfirmPassword.isSecureTextEntry = false
            btnConfirmPasswordEye.setImage(#imageLiteral(resourceName: "hide"), for: .selected)
            btnConfirmPasswordEye.isSelected = false
        } else {
            txtConfirmPassword.isSecureTextEntry = true
            btnConfirmPasswordEye.setImage(#imageLiteral(resourceName: "unHide"), for: .normal)
            btnConfirmPasswordEye.isSelected = true
        }
        
        iconClickConfirmPass = !iconClickConfirmPass
    }
    
    
    @IBAction func btnCreateAccountAction(_ sender: Any) {
        if (txtName.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter name.")
        }
        else if (txtEmail.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter email.")
        }
        else if (txtEmail.text?.isValidEmail() == false) {
            
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Valid email.")
        }
        else if (txtMobileNumber.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter phone number.")
        }
        else if (txtPassword.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter Password.")
        }
        else if(checkForSpace(strCheckString: txtPassword.text!) ==  true){
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "White space not allowed in the password field.")
        }
        else if txtConfirmPassword.text! != txtPassword.text! {
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Password does not match the confirm password.")
        }
        else {
            callWebServiceForRegister()
        }
    }
    
    //MARK: - Custom Methods
    func checkForSpace(strCheckString : String) -> Bool {
        
        let whiteSpace = " "
        if strCheckString.contains(whiteSpace) {
            print(strCheckString)
            print ("has whitespace")
            return true
        } else {
            print(strCheckString)
            print("no whitespace")
            return false
        }
    }
    
    func setLoginData(json: JSON) {
        
        let auth = AuthModel.sharedInstance
        auth.email = json["data"]["email"].stringValue
        auth.user_id = json["data"]["id"].stringValue
        auth.mobile = json["data"]["mobile"].stringValue
        auth.name = json["data"]["name"].stringValue
        auth.userType = json["data"]["user_type"].stringValue
        auth.createdAt = json["data"]["created_at"].stringValue
        auth.deviceToken = json["data"]["device_token"].stringValue
        auth.latitude = json["data"]["latitude"].stringValue
        auth.longitude = json["data"]["longitude"].stringValue
        auth.password = json["data"]["password"].stringValue

        let defaults = UserDefaults.standard
        defaults.set(auth.email, forKey: "email")
        defaults.set(auth.user_id, forKey: "user_id")
        defaults.set(auth.mobile, forKey: "mobile")
        defaults.set(auth.name, forKey: "name")
        defaults.set(auth.userType, forKey: "user_type")
        defaults.set(auth.createdAt, forKey: "created_at")
        defaults.set(auth.deviceToken, forKey: "device_token")
        defaults.set(auth.latitude, forKey: "latitude")
        defaults.set(auth.longitude, forKey: "longitude")
        defaults.set(auth.password, forKey: "password")

    }
    
    //MARK: - CLLocationManager Delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locValue = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        locationManager.stopUpdatingLocation()
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(image, 1.0)!
        if let imageData = image.jpeg(.medium) {
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }

    //MARK: - CallWebService
    func callWebServiceForRegister() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
       
        let param = ["name": txtName.text!,
                     "email":txtEmail.text ?? "",
                     "mobile": txtMobileNumber.text ?? "",
                     "password":txtPassword.text ?? "",
                     "latitude":String(locValue.latitude),
                     "longitude":String(locValue.longitude),
                     "device_token":"",
                     "device_id":UIDevice.current.identifierForVendor!.uuidString,
                     "organisation_id": AuthModel.sharedInstance.orgId,
                     "image":isProfilePictureSelected ? convertImageToBase64(image: imgProfile.image!) : ""
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.register, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["code"].stringValue == "200" {
                    //self.setLoginData(json: response!)
                    
                    if UserDefaults.standard.value(forKey: "user_type") as! String == "3" {
                        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LocationSwitchView") as! LocationSwitchView
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    else {
                        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UserListView") as! UserListView
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    //MARK: - UITextField Delegates
   
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }

    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        textField.placeHolderColor = UIColor.darkGray
        
        if textField == txtName{
            
            viewName.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
        else if textField == txtEmail{
            
            viewEmail.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
       
            
        else if textField == txtMobileNumber{
            
            viewPhoneNo.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
        else if textField == txtPassword{
            
            viewPassword.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
            
        else if textField == txtConfirmPassword{
            
            viewConfirmPassword.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.placeHolderColor = UIColor.darkGray
         NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
        
        if textField == txtName {
            
            viewName.borderColor = UIColor.lightGray
        }
        else if textField == txtEmail{
            
            viewEmail.borderColor = UIColor.lightGray
        }
       
        else if textField == txtMobileNumber{
            
            viewPhoneNo.borderColor = UIColor.lightGray
        }
        else if textField == txtPassword{
            
            viewPassword.borderColor = UIColor.lightGray
        }
            
        else if textField == txtConfirmPassword{
            
            viewConfirmPassword.borderColor = UIColor.lightGray
        }
        
    }
    
    @IBAction func btnCameraAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            isProfilePictureSelected = true
            imgProfile.image = image
            self.dismiss(animated: false, completion: nil)
        }
        else{
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    
}
