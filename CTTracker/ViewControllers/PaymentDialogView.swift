//
//  PaymentDialogView.swift
//  CTTracker
//
//  Created by Apple on 17/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import JKNotificationPanel
class PaymentDialogView: UIViewController {

    let panel = JKNotificationPanel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btnProceedAction(_ sender: Any) {
        
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
}
