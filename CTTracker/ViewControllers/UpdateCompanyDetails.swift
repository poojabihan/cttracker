 //
 //  UpdateCompanyDetails.swift
 //  CTTracker
 //
 //  Created by Apple on 08/01/19.
 //  Copyright © 2019 Apple. All rights reserved.
 //
 
 import UIKit
 import JKNotificationPanel
 import NVActivityIndicatorView
 import SwiftyJSON
 
 
 class UpdateCompanyDetails: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    
    //MARK: - IBOutlet
    @IBOutlet weak var txtCName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtWebsite: UITextField!
    
    @IBOutlet weak var viewCName: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPhoneNo: UIView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewWebsite: UIView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    
    //MARK: - Variable
    let panel = JKNotificationPanel()
    var imagePicker = UIImagePickerController()
    var textField: UITextField?
    var strUpdate = ""
    
    //MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEmail.placeHolderColor = UIColor.darkGray
        txtPhoneNo.placeHolderColor = UIColor.darkGray
        txtCName.placeHolderColor = UIColor.darkGray
        txtAddress.placeHolderColor = UIColor.darkGray
        txtWebsite.placeHolderColor = UIColor.darkGray
        
      
        // Notification Observers for UIKeyboard Hide Unhide
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterCompanyView.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterCompanyView.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        imagePicker.delegate = self
        txtEmail.isUserInteractionEnabled = false
        viewEmail.alpha = 0.6
        
        txtCName.text = AuthModel.sharedInstance.name
        txtEmail.text = AuthModel.sharedInstance.email
        txtPhoneNo.text = AuthModel.sharedInstance.mobile
        txtAddress.text = AuthModel.sharedInstance.address
        txtWebsite.text = AuthModel.sharedInstance.website
        
        let url = URL(string: KImageURL + AuthModel.sharedInstance.image)
        
        imgProfile.kf.setImage(with: url, placeholder: UIImage(named : "placeholder"), progressBlock:nil, completionHandler: nil)
    }
    //MARK: - UITextfiled Delegete
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.placeHolderColor = UIColor.darkGray
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidShow, object: self)
        
        // Decipad config for adding Done button above itself
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let flexiableSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked))
        toolBar.setItems([flexiableSpace, doneButton], animated: false)
        
        if textField == txtEmail{
            
            viewEmail.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
        else if textField == txtCName{
            
            viewCName.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
            
        else if textField == txtAddress{
            
            viewAddress.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
        else if textField == txtWebsite{
            
            viewWebsite.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
            
        else if textField == txtPhoneNo{
            
            viewPhoneNo.borderColor = UIColor.init(red: 236.0/225.0, green: 28.0/225.0, blue: 45.0/225.0, alpha: 1.0)
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.placeHolderColor = UIColor.darkGray
        NotificationCenter.default.post(name: Notification.Name.UIKeyboardDidHide, object: self)
        
        if textField == txtEmail{
            
            viewEmail.borderColor = UIColor.lightGray
        }
        else if textField == txtCName{
            
            viewCName.borderColor = UIColor.lightGray
        }
            
        else if textField == txtAddress{
            
            viewAddress.borderColor = UIColor.lightGray
        }
        else if textField == txtWebsite{
            
            viewWebsite.borderColor = UIColor.lightGray
        }
            
        else if textField == txtPhoneNo{
            
            viewPhoneNo.borderColor = UIColor.lightGray
        }
        
    }
    
    //MARK: - IBAction
    
   
    @IBAction func btnRegisterCompanyAction(_ sender: Any) {
        
        if (txtCName.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter company name.")
        }
            
        else if (txtEmail.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter email.")
        }
        else if (txtEmail.text?.isValidEmail() == false) {
            
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter valid email.")
        }
        else if (txtPhoneNo.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter phone number.")
        }
        else if (txtAddress.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter address.")
        }
        else if (txtWebsite.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Please enter company website.")
        }
            
        else {
            callWebServiceForUpdateCompanyDetails()
        }
        
    }
    
    @IBAction func btnCameraAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose your file from any of this options", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: - Custom Functions
    
    /*Action Sheet Options Function for Uploading File*/
    
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(image, 1.0)!
        if let imageData = image.jpeg(.medium) {
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }

    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imgProfile.image = image
            self.dismiss(animated: false, completion: nil)
        }
            
        else{
            
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "This image will not support.")
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    //MARK: - UITextFiled keyboard hide unhide methods
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tblView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.tblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    // Decipad config for adding Done button above itself
    @objc func doneClicked(){
        textField?.resignFirstResponder()
        textField?.endEditing(true)
    }
    
    
    func  callWebServiceForUpdateCompanyDetails(){
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        /*User Type
         1 - super admin
         2 - organisation admin
         3 - user
         */
        
        var param = [String : Any]()
        
//        if AuthModel.sharedInstance.userType == "3"{
//
//            param = ["name": txtCName.text ?? "",
//                     "mobile": txtPhoneNo.text ?? "",
//                     "image":  convertImageToBase64(image: imgProfile.image!) ?? "",
//                     "userId": AuthModel.sharedInstance.user_id,
//                     "address": "",
//                     "website": ""]
//        }
//
//        else{
            param = ["name": txtCName.text ?? "",
                     "mobile": txtPhoneNo.text ?? "",
                     "address":txtAddress.text ?? "",
                     "website":txtWebsite.text ?? "",
                     "image":  convertImageToBase64(image: imgProfile.image!) ,
                     "userId": AuthModel.sharedInstance.user_id]
     //   }
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.updateUserProfile, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["code"].stringValue == "200" {
                    self.setLoginData(json: response!)
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: "Details Update Successfully.")
                    self.navigationController?.popViewController(animated: true)
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
        
    }
    //MARK: - Custom Methods
    func setLoginData(json: JSON) {
        
        let auth = AuthModel.sharedInstance
        auth.email = json["data"]["email"].stringValue
        auth.user_id = json["data"]["id"].stringValue
        auth.mobile = json["data"]["mobile"].stringValue
        auth.name = json["data"]["name"].stringValue
        auth.userType = json["data"]["user_type"].stringValue
        auth.orgId = json["data"]["organisation_id"].stringValue
        auth.image = json["data"]["image"].stringValue
        auth.address = json["data"]["address"].stringValue
        auth.website = json["data"]["website"].stringValue
        
        let defaults = UserDefaults.standard
        defaults.set(auth.email, forKey: "email")
        defaults.set(auth.user_id, forKey: "user_id")
        defaults.set(auth.mobile, forKey: "mobile")
        defaults.set(auth.name, forKey: "name")
        defaults.set(auth.userType, forKey: "user_type")
        defaults.set(auth.orgId, forKey: "organisation_id")
        defaults.set(auth.image, forKey: "image")
        defaults.set(auth.address, forKey: "address")
        defaults.set(auth.website, forKey: "website")
    }
    
    
 }
