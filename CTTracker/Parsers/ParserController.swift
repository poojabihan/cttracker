//
//  ParserController.swift
//  CTTracker
//
//  Created by Apple on 11/12/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import SwiftyJSON

class ParserController: NSObject {

    class func parseUserList(response : JSON, completionHandler: @escaping ([UserModel], String) -> Void) {
        
        var arrModel = [UserModel]()
        
        for json in response["data"].arrayValue {
            let model = UserModel()
            
            model.strUserID = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strEmail = json["email"].stringValue
            model.strMobile = json["mobile"].stringValue
            model.strImage = json["image"].stringValue

            arrModel.append(model)
        }
        
        completionHandler(arrModel, response["total"].stringValue)
    }
    
    class func parseLocationList(response : JSON, completionHandler: @escaping ([LocationModel], String) -> Void) {
        
        var arrModel = [LocationModel]()
        
        for json in response["data"].arrayValue {
            let model = LocationModel()
            
            model.strLatitude = json["latitude"].stringValue
            model.strLongitude = json["longitude"].stringValue
            model.strAddress = json["address"].stringValue == "" ? "     " : json["address"].stringValue
            model.strDate = json["date"].stringValue
            
            arrModel.append(model)
        }
        
        completionHandler(arrModel, response["total"].stringValue)
    }
    
    class func parseLocationMapList(response : JSON, completionHandler: @escaping ([LocationModel]) -> Void) {
        
        var arrModel = [LocationModel]()
        
        for json in response["data"].arrayValue {
            let model = LocationModel()
            
            model.strLatitude = json["latitude"].stringValue
            model.strLongitude = json["longitude"].stringValue
            model.strAddress = json["address"].stringValue == "" ? "     " : json["address"].stringValue
            model.strDate = json["date"].stringValue
            
            arrModel.append(model)
        }
        
        completionHandler(arrModel)
    }
}
