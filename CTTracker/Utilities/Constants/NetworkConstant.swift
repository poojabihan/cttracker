//
//  NetworkConstant.swift
//  Tribe365
//
//  Created by kdstudio on 30/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit


let kBaseURL = "http://production.chetaru.co.uk/tracker/api/"
let KImageURL = "http://production.chetaru.co.uk/tracker/public/uploads/"
class NetworkConstant: NSObject {
    
        static let login = "userLogin"
        static let saveLocation = "saveLocation"
        static let loginStatus = "loginStatus"
        static let register = "userRegister"
        static let userList = "getUsers?page="
        static let locationList = "getUserLocation"
        static let organisationRegister = "organisationRegister"
        static let updateUserProfile = "updateUserProfile"
        static let userUpdatePassword = "userUpdatePassword"
       static let getUserLocationMap = "getUserLocationMap"
        static let forgotPassword = "forgotPassword"

}

