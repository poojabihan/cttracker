//
//  WebServiceHandler.swift
//  Tribe365
//
//  Created by kdstudio on 29/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class WebServiceHandler: NSObject {
    
    class func postWebService(url:String, param:[String:Any], withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
        let defaults = UserDefaults.standard
        
        let token = withHeader ? ("Bearer " + (defaults.value(forKey: "token") as! String)) : ""
        
        let headers = withHeader ? ["Authorization":token,"Accept":"application/json","Content-Type":"application/json"] : nil
        
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }

    }

    class func postWebServiceForHtml(url:String, param:[String:Any], withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
       
        let headers =  ["Content-Type":"text/html"]
        
        Alamofire.request(url, method: .post, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
        
    }
    
    class func getWebServiceForMultiplePart(url:String, param:[String:Any]?, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void  {
        
        Alamofire.request(url, method : .get, parameters: param).responseJSON { response in
           //  let headers = ["Content-Type":"application/json"]
        
           //  Alamofire.request(url, method: .get, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,nil)
                break
            case .failure(let error):
                print("Error: " + error.localizedDescription)
                completionHandler(nil,error.localizedDescription)

                break
            }
        }
    }
    
    class func getWebService(url:String, param:[String:Any]?, withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
        let defaults = UserDefaults.standard
        
        let token = withHeader ? ("Bearer " + (defaults.value(forKey: "token") as! String)) : ""
        
        let headers = withHeader ? ["Authorization":token,"Accept":"application/json","Content-Type":"application/json"] : nil
        
        Alamofire.request(url, method: .get, parameters: param, encoding: JSONEncoding.default, headers: withHeader ? headers : nil).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
        
    }
    
    
    
    class func delelteWebService(url:String, param:[String:Any]?, withHeader: Bool, completionHandler: @escaping (JSON?, String?) -> Void) ->  Void {
        
      
        
        let defaults = UserDefaults.standard
        let token = "Bearer " + (defaults.value(forKey: "token") as! String)
        let headers = ["Authorization":token,"Accept":"application/json"]
        
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
    
            switch response.result {
            case .success(let value):
                print(response)
                let json = JSON(value)
                completionHandler(json,json["message"].stringValue)
                break
            case .failure(let error):
                completionHandler(nil,error.localizedDescription)
            }
        }
    }
}
